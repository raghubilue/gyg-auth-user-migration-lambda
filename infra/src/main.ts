#!/usr/bin/env node

import { App } from '@aws-cdk/core';
import MigrationLambda from './migration-lambda';

const app = new App();

new MigrationLambda(app, 'auth-user-migration-stack');

app.synth();
