import { App, CfnOutput, Duration, Fn, Stack } from '@aws-cdk/core';
import * as apiGateway from '@aws-cdk/aws-apigateway';
import * as lambda from '@aws-cdk/aws-lambda';

export default class MigrationLambda extends Stack {
  restApi: apiGateway.RestApi;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  constructor(scope: App, id: string, props?: any) {
    super(scope, id, props);

    const app = this.node.root as App;

    const restapi = apiGateway.RestApi.fromRestApiAttributes(this, 'migration', {
      restApiId: Fn.importValue('ExtRestApiId'),
      rootResourceId: Fn.importValue('ExtRestApiRootResourceId'),
    });

    const lambdaFunction = new lambda.Function(this, 'auth-user-migration', {
      runtime: lambda.Runtime.NODEJS_12_X,
      handler: 'gyg-auth-user-migration/index.lambdaHandler',
      code: lambda.Code.fromAsset('../dist/gyg-auth-user-migration.zip'),
    });

    restapi.root.addResource('getToken').addMethod('GET', new apiGateway.LambdaIntegration(lambdaFunction));
  }
}
