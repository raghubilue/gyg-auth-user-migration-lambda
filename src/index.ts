import { APIGatewayEvent, APIGatewayProxyEvent } from 'aws-lambda';
import * as jsWt from 'jsonwebtoken';
interface AuthorizationHeaders {
  Authorization: string | void;
  authorization: string | void;
}

type AuthorizerEvent = APIGatewayProxyEvent & AuthorizationHeaders;

export const lambdaHandler = async (event: APIGatewayEvent) => {
  const response = {
    statusCode: 200,
    body: 'Hello world from <LAMBDA_NAME> Lambda',
  };
  return response;
};
