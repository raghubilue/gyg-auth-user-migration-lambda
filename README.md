## How to deploy

The IAAC to deploy lambda is written in CDK. Hence we would be using the cdk command to deploy lambda.

### Pre-requisites

Configure AWS credentials as per your account. Ensure the credentials provided has ability to execute cloudformation and create IAM roles as part of cloudformation.

### Commands to deploy

Build Lambda Code: Since the lambda code is separate from cdk code, we need to zip the lambda code by running "npm run build". This command generates zip file under dist folder on the root.
Deploy using cdk: After the zip file is ready, navigate to /infra folder and execute the following commands

```
"cdk bootstrap"
"cdk deploy auth-user-migration-stack"
```
